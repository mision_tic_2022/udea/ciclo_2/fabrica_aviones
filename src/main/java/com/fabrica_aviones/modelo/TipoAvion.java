package com.fabrica_aviones.modelo;

public enum TipoAvion {
    AVION_CARGA,
    AVION_PASAJEROS,
    AVION_MILITAR
}
