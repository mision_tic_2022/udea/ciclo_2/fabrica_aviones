package com.fabrica_aviones.modelo;

public class AvionPasajeros extends Avion {
    //ATRIBUTOS
    private int pasajeros;

    //CONSTRUCTOR
    public AvionPasajeros(int pasajeros, String color, double tamanio){
        //Enviar parámetros al constructor de la superClase Avion
        super(color, tamanio);
        this.pasajeros = pasajeros;
    }

    @Override
    public String toString() {
        String info = "-----------------AVION DE PASAJEROS-------------\n";
        info += "Color: "+getColor();
        info += "\nTamaño: "+getTamanio();
        info += "\nCantidad pasajeros: "+pasajeros;
        return info;
    }

    public int getPasajeros(){
        return pasajeros;
    }

    public void setPasajeros(int pasajeros){
        this.pasajeros = pasajeros;
    }

    //ACCIONES
    public void servir(){
        System.out.println("Sirviendo alimentos a los pasajeros...👮‍♀️");
    }
}
