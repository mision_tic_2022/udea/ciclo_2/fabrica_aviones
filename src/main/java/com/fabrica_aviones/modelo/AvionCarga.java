package com.fabrica_aviones.modelo;

public class AvionCarga extends Avion {
    
    //CONSTRUCTOR
    public AvionCarga(String color, double tamanio){
        //Llamar al método constructor de la super clase 'Avion'
        super(color, tamanio);
    }

    @Override
    public String toString() {
        String info = "-----------------AVION DE CARGA-------------\n";
        info += "Color: "+getColor();
        info += "\nTamaño: "+getTamanio();
        return info;
    }

    //ACCIONES
    public void cargar(){
        System.out.println("Cargando...");
    }

    public void descargar(){
        System.out.println("Descargar...");
    }

}
